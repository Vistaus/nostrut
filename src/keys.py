'''
 Copyright (C) 2023  Lennart Kroll

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 nostrut is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import pyotherside
from nostr_sdk import Keys, SecretKey

def generate_publickey(input):
    try:
        keys = Keys.from_sk_str(input)
    except:
        pyotherside.send("invalid_privatekey")
        return

    pyotherside.send("publickey_generated", keys.secret_key().to_bech32(), keys.public_key().to_bech32(), keys.public_key().to_hex())

def generate_keys():
    keys = Keys.generate()
    return [keys.secret_key().to_bech32(), keys.public_key().to_bech32(), keys.public_key().to_hex()]
