import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

BottomEdge {
    id: bottomEdge
    height: contentItem ? contentItem.height : 0

    onCommitCompleted: contentItem.focusTextArea()

    contentComponent: Rectangle {
        id: publishNoteRectangle

        property bool publishing: false
        property bool focused: false

        function focusTextArea() {
            noteTextArea.focus = true;
        }

        width: mainPage.width
        height: noteTextArea.height + units.gu(4)
        color: theme.palette.normal.background

        ProgressBar {
            id: progressBar
            indeterminate: true
            visible: publishing
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
        }

        TextArea {
            id: noteTextArea
            autoSize: true
            maximumLineCount: 10
            anchors.centerIn: parent
            width: parent.width - units.gu(12)
            placeholderText: i18n.tr("Post something awesome today")
        }

        Button {
            iconName: "send"
            width: units.gu(4)
            height: units.gu(4)
            color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.porcelain : LomiriColors.inkstone
            anchors {
                right: parent.right
                rightMargin: units.gu(1)
                verticalCenter: parent.verticalCenter
            }
            onClicked: {
                if (noteTextArea.text) {
                    publishing = true;
                    python.call("client.publish_note", [keySettings.privateKey, noteTextArea.text], function(){
                        noteTextArea.text = "";
                        publishing = false;
                        bottomEdge.collapse();
                    });
                }
            }
        }

        Button {
            iconName: "close"
            width: units.gu(4)
            height: units.gu(4)
            color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.porcelain : LomiriColors.inkstone
            anchors {
                left: parent.left
                leftMargin: units.gu(1)
                verticalCenter: parent.verticalCenter
            }
            onClicked: bottomEdge.collapse()
        }
    }
}
