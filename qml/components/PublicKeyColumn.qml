import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Column {
    visible: keySettings.keySet
    spacing: units.gu(1)
    Label {
        text: "Public Key:"
    }
    Row {
        spacing: units.gu(2)
        Label {
            text: keySettings.publicKey
            width: keySettingsColumn.width - copyPublicKeyIcon.width - units.gu(2)
            wrapMode: Label.WrapAnywhere
        }
        Icon {
            id: copyPublicKeyIcon
            name: "edit-copy"
            width: units.gu(3)

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Clipboard.push(keySettings.publicKey);
                    clipboardPopup.content = "Public Key copied";
                    clipboardPopup.visible = true;
                    hideClipboardPopupTimer.start()
                }
            }
        }
    }
}
