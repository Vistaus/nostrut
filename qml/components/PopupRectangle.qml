import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Rectangle {
    property string content: ""

    visible: false
    color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.silk : LomiriColors.slate
    width: units.gu(30)
    height: units.gu(4)
    radius: units.gu(2)
    Label {
        id: popupLabel
        anchors.centerIn: parent
        text: content
    }
}
