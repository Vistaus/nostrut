import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Component {
    id: resetDialogPopup
    Dialog {
        id: resetDialog
        title: i18n.tr("Reset keys?")
        text: i18n.tr("Resetting keys can lock you out of your account, continue?\n\nNOTE: This will not delete your account or any of your posts")
        Button {
            text: i18n.tr("Reset keys")
            color: LomiriColors.red
            onClicked: {
                keySettings.privateKey = "";
                keySettings.publicKey = "";
                keySettings.publicKeyHex = "";
                keySettings.keySet = false;
                general.setupCompleted = false;
                root.subs = [];
                root.subs_metadata = [];
                mainStack.clear();
                mainStack.push(Qt.resolvedUrl("../pages/SetupPage.qml"));
                PopupUtils.close(resetDialog)
            }
        }
        Button {
            text: i18n.tr("Cancel")
            onClicked: PopupUtils.close(resetDialog)
        }
    }
}
