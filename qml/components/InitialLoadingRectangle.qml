import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Rectangle {
    id: initialLoadingRectangle
    visible: false

    color: theme.palette.normal.background
    ActivityIndicator {
        id: initialLoadingActivityIndicator
        anchors {
            centerIn: parent
            verticalCenterOffset: units.gu(-5)
        }
        running: true
    }
    Label {
        anchors {
           top: initialLoadingActivityIndicator.bottom
           topMargin: units.gu(2)
           horizontalCenter: parent.horizontalCenter
        }
        text: i18n.tr("Connecting to Relays")
    }
}
