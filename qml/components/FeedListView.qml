import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

ListView {
    id: feedListView

    property int likeFetchCounter: 4

    spacing: units.gu(2)

    model: feedListModel

    onAtYEndChanged: {
        if (atYEnd && eventsFetched && root.subs.length > 0 && feedListModel.count > 5 && !loadingMoreEvents) {
          // console.log("loading more posts");
          loadingMoreEvents = true;
          mainPage.until = mainPage.since;
          mainPage.since = mainPage.until - mainPage.fetch_time;
          mainPage.oldEventCount = feedListModel.count;
          python.call("client.fetch_events", [root.subs, root.subs_metadata, mainPage.since, mainPage.until]);
        }
    }

    onContentYChanged: {
        let index = indexAt(0, contentY);
        if (index > 0 && !mainPage.goUp) {
            mainPage.goUp = true;
        }
        else if (index <= 0 && mainPage.goUp && index != -1){
            mainPage.goUp = false;
        }

        let index2 = indexAt(0, contentY + feedListView.height);
        if (index2 > 4 && index2 > likeFetchCounter) {
            likeFetchCounter = index2;
            // console.log(index2);
            python.call("client.fetch_reactions", [feedListModel.get(index2).hex_event_id, feedListModel.get(index2).hex_pubkey, "events"]);
            python.call("client.fetch_reply_count", [feedListModel.get(index2).hex_event_id, feedListModel.get(index2).hex_pubkey, 1, "events"]);
            // console.log(feedListModel.get(index2).hex_event_id);
            // console.log(feedListModel.get(index2).hex_pubkey);
        }
    }

    PullToRefresh {
        id: pullToRefresh
        visible: eventsFetched
        refreshing: !eventsFetched
        onRefresh: {
            console.log("reloading posts");
            eventsFetched = false;
            feedListModel.clear();
            mainPage.fetch_time = 15*60;
            mainPage.until = Math.floor(Date.now() / 1000);
            mainPage.since = mainPage.until - mainPage.fetch_time;
            likeFetchCounter = 4;
            mainPage.likeFetchCounter = 0;
            python.call("client.refetch_events", [keySettings.privateKey, mainPage.since, mainPage.until]);
        }
    }

    delegate: FeedListItem {
        onClicked: {
            mainStack.push(Qt.resolvedUrl("../pages/ReplyPage.qml"), {
                "selectedNote" : feedListModel.get(index),
                "depth": 1
            });
        }
    }
}
