import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import "../js/functions.js" as Functions

Column {
    id: note
    spacing: units.gu(2)

    Row {
        anchors {
            left: parent.left
            right: parent.right
        }
        spacing: units.gu(2)
        Label {
            text: combinedName
            textSize: Label.Small
            wrapMode: Label.WrapAnywhere
            width: parent.width - dateLabel.width - units.gu(2)
            MouseArea {
                enabled: profileClickable
                anchors.fill: parent
                onClicked: {
                    mainStack.push(Qt.resolvedUrl("../pages/ProfilePage.qml"), {
                        "hex_pubkey": hex_pubkey,
                        "npub": npub,
                        "name": name,
                        "display_name": display_name,
                        "picture": picture,
                        "banner": banner,
                        "banner": banner,
                        "website": website,
                        "about": about
                    });
                }
            }
        }
        Label {
            id: dateLabel
            text: date
            textSize: Label.Small
        }
    }
    Row {
        width: parent.width
        spacing: units.gu(2)
        Image {
            id: profilePicture
            source: picture ? picture : "../../assets/nostr_logo_wht_grey.png"
            width: units.gu(6)
            fillMode: Image.PreserveAspectFit
            MouseArea {
                enabled: profileClickable
                anchors.fill: parent
                onClicked: {
                    mainStack.push(Qt.resolvedUrl("../pages/ProfilePage.qml"), {
                        "hex_pubkey": hex_pubkey,
                        "npub": npub,
                        "name": name,
                        "display_name": display_name,
                        "picture": picture,
                        "banner": banner,
                        "banner": banner,
                        "website": website,
                        "about": about
                    });
                }
            }
        }
        Column {
            anchors.verticalCenter: parent.verticalCenter
            spacing: units.gu(2)
            Label {
                visible: content
                text: content
                textSize: Label.Medium
                textFormat: Label.RichText
                wrapMode: Label.Wrap
                width: note.width - profilePicture.width - units.gu(2)
                onLinkActivated: Qt.openUrlExternally(link)
            }
            Column {
                id: imageColumn
                spacing: units.gu(2)
                Repeater {
                    model: imgURLs
                    delegate: Image {
                        source: imgURL
                        fillMode: Image.PreserveAspectFit
                        width: note.width - profilePicture.width - units.gu(4)
                        MouseArea {
                            anchors.fill: parent
                            onClicked: mainStack.push(Qt.resolvedUrl("../pages/ImagePage.qml"), {"imgURL": imgURL})
                        }
                    }
                }
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)
        Row {
            spacing: units.gu(1)
            Icon {
                id: likeIcon
                name: liked ? "like" : "unlike"
                width: units.gu(2.5)
                height: width
                color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.slate : LomiriColors.silk
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (!likeInitiated && !liked) {
                            likeInitiated = true;
                            python.call("client.add_reaction", [keySettings.privateKey, hex_event_id, hex_pubkey], function(){
                                likes += 1;
                                // likeIcon.color = LomiriColors.red;
                                liked = true;
                            })
                        }
                    }
                }
            }
            Label {
                text: likes
                anchors.verticalCenter: parent.verticalCenter
            }
        }
        Row {
            spacing: units.gu(1)
            Icon {
                id: replyIcon
                name: "messages"
                width: units.gu(2.5)
                height: width
            }
            Label {
                text: replies
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }

    Row {
        visible: replying
        width: parent.width
        spacing: units.gu(1)

        /*
        onVisibleChanged: {
            if (visible) {
                replyTextArea.focus = true;
            }
        }
        */

        Button {
            id: closeButton
            iconName: "close"
            width: units.gu(4)
            height: width
            color: theme.palette.normal.background
            anchors.verticalCenter: replyTextArea.verticalCenter

            onClicked: {
                replying = false;
            }
        }
        TextArea {
            id: replyTextArea
            autoSize: true
            maximumLineCount: 5
            placeholderText: i18n.tr("Reply something nice")
            width: parent.width - closeButton.width - sendButton.width - units.gu(2)
        }
        Button {
            id: sendButton
            visible: !sending
            iconName: "send"
            width: units.gu(4)
            height: width
            color: theme.palette.normal.background
            anchors.verticalCenter: replyTextArea.verticalCenter

            onClicked: {
                if (replyTextArea.text) {
                    sending = true;
                    python.call("client.add_reply", [keySettings.privateKey, json, replyTextArea.text], function(){
                        replyTextArea.text = "";
                        sending = false;
                        replying = false;
                        if (String(mainStack.currentPage).startsWith("ReplyPage") && isParent) {
                            console.log("reloading replies");
                            repliesFetched = false;
                            replyPageListModel.clear();
                            replyPageListModel.append(selectedNote);
                            until = Math.floor(Date.now() / 1000);
                            since = until - fetch_time;
                            python.call("client.fetch_replies", [selectedNote.hex_event_id, selectedNote.hex_pubkey, since, until, depth]);
                        }
                    });
                }
            }
        }
        ActivityIndicator {
            id: sendingIndicator
            visible: sending
            running: sending
            width: units.gu(3)
            height: width
            anchors.verticalCenter: replyTextArea.verticalCenter
        }
    }
}
