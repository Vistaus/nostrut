import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Column {
    property string heading: ""
    property string iconName: ""
    property string content: ""

    width: parent.width
    spacing: units.gu(1)
    Row {
        spacing: units.gu(0.5)
        Icon {
            height: units.gu(2)
            name: iconName
        }
        Label {
            text: heading
        }
    }
    Row {
        width: parent.width
        spacing: units.gu(1)
        Label {
            width: parent.width - copyIcon.width - units.gu(1)
            text: content
            wrapMode: Label.WrapAnywhere
            textSize: Label.Small
        }
        Icon {
            id: copyIcon
            name: "edit-copy"
            height: units.gu(3)

            MouseArea {
                anchors.fill: parent
                onClicked: Clipboard.push(content)
            }
        }
    }
}
