/*
 * Copyright (C) 2023  Lennart Kroll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * nostrut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

MainView {
    id: root
    objectName: "mainView"

    width: units.gu(45)
    height: units.gu(75)
    visible: true

    anchorToKeyboard: true

    property var subs: []
    property var subs_metadata: []

    Settings {
        id: general
        category: "General"
        property bool setupCompleted: false
    }

    Settings {
        id: keySettings
        category: "KeySettings"
        property string privateKey: ""
        property string publicKey: ""
        property string publicKeyHex: ""
        property bool keySet: false
    }

    Settings {
        id: relaySettings
        category: "RelaySettings"
        property var relays: ["wss://example.relay.com"]
    }

    PageStack {
        id: mainStack
        Component.onCompleted: {
            if (general.setupCompleted) {
                mainStack.push(Qt.resolvedUrl("pages/MainPage.qml"));
            }
            else {
                mainStack.push(Qt.resolvedUrl("pages/SetupPage.qml"));
            }
        }
    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl("../src/"));
            importModule("client", function(){});
            importModule("keys", function(){});

            setHandler("print", function(message){
                console.log(message);
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
