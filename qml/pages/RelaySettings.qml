import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    id: relaySettingsPage

    header: PageHeader {
        id: settingsHeader
        title: i18n.tr("Relays")
    }

    Label {
        anchors.centerIn: parent
        text: i18n.tr("Coming Soon")
    }
}
