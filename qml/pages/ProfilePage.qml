import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import "../components"
import "../js/functions.js" as Functions

Page {
    id: profilePage

    property string banner: ""
    property string picture: ""
    property string name: ""
    property string display_name: ""
    property string combinedName: ""
    property string npub: ""
    property string website: ""
    property string about: ""
    property string hex_pubkey: ""
    property bool following
    property bool postsFetched: false
    property bool loadingMorePosts: false
    property int fetch_time: 15*60 // Timeframe to fetch text events from as unix timestamp
    property int until
    property int since
    property int oldPostCount: 0
    property int followingCount: 0
    property int followersCount: 0
    property bool goUp: false
    property int replyCount // count of replies to be deleted
    property int postCount: 0 // count of posts to be deleted

    signal clearUnwantedPosts(var count)

    Component.onCompleted: {
        if (root.subs.includes(hex_pubkey)) {
            following = true;
        }
        else {
            following = false;
        }

        if (npub == keySettings.publicKey) {
            followingCount = root.subs.length;
            python.call("client.get_followers", [hex_pubkey], function(followers) {
                followersCount = followers.length;
                combinedName = Functions.getCombinedName2(name, display_name);
                until = Math.floor(Date.now() / 1000);
                since = until - fetch_time;
                python.call("client.fetch_posts", [hex_pubkey, since, until]);
            });
        }
        else {
            python.call("client.get_subs", [hex_pubkey], function(subs) {
                followingCount = subs.length;
                python.call("client.get_followers", [hex_pubkey], function(followers) {
                    followersCount = followers.length;
                    combinedName = Functions.getCombinedName2(name, display_name);
                    until = Math.floor(Date.now() / 1000);
                    since = until - fetch_time;
                    python.call("client.fetch_posts", [hex_pubkey, since, until]);
                });
            });
        }
    }

    Component.onDestruction: {
        if (String(mainStack.currentPage).startsWith("ReplyPage")) {
            mainStack.currentPage.postCount = postCount + postListModel.count;
            mainStack.currentPage.clearUnwantedReplies(replyCount);
            console.log("Deleting " + replyCount + " unwanted Replies");
        }
    }

    header: PageHeader {
        id: settingsHeader
        title: combinedName ? combinedName : i18n.tr("Profile")

        trailingActionBar {
            actions: [
                Action {
                    iconName: "external-link"
                    onTriggered: Qt.openUrlExternally("https://njump.me/" + npub)
                }
            ]
        }
    }

    ProgressBar {
        z: 1
        id: progressBar
        indeterminate: true
        visible: !postsFetched || loadingMorePosts
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
        }
    }

    BackToTopRectangle {
        MouseArea {
            anchors.fill: parent
            onClicked: profilePageScrollView.flickableItem.contentY = 0
        }
    }

    ScrollView {
        id: profilePageScrollView
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        contentItem: Rectangle {
            color: theme.palette.normal.background
            width: profilePageScrollView.width
            height: bannerImage.height + profileColumn.height + aboutRectangle.height + followButton.height + postListView.height + units.gu(8)

            Image {
                id: bannerImage
                anchors {
                    top: parent.top
                    right: parent.right
                    left: parent.left
                }
                source: banner
                height: banner ? units.gu(12) : 0
                fillMode: Image.PreserveAspectCrop
                MouseArea {
                    anchors.fill: parent
                    onClicked: mainStack.push(Qt.resolvedUrl("../pages/ImagePage.qml"), {"imgURL": banner, "imageTitle": i18n.tr("Banner")})
                }
            }

            Column {
                id: profileColumn
                height: profileColumn.contentHeight
                anchors {
                    top: banner ? bannerImage.bottom : parent.top
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }
                spacing: units.gu(2)
                Row {
                    spacing: units.gu(2)
                    Image {
                        id: profilePic
                        source: picture ? picture : "../../assets/nostr_logo_wht_grey.png"
                        width: units.gu(10)
                        fillMode: Image.PreserveAspectFit
                        MouseArea {
                            anchors.fill: parent
                            onClicked: mainStack.push(Qt.resolvedUrl("../pages/ImagePage.qml"), {"imgURL": picture, "imageTitle": i18n.tr("Profile Picture")})
                        }
                    }
                    Column {
                        anchors.verticalCenter: parent.verticalCenter
                        spacing: units.gu(1)
                        Label {
                            id: profileName
                            text: combinedName
                        }
                        Label {
                            id: profileNpub
                            text: npub
                            width: profilePage.width - profilePic.width - units.gu(8)
                            wrapMode: Label.WrapAnywhere
                            textSize: Label.Small
                        }
                    }
                }
            }

            Rectangle {
                id: aboutRectangle
                visible: website || about
                anchors {
                    top: profileColumn.bottom
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }
                width: profileColumn.width
                height: visible ? aboutColumn.implicitHeight + units.gu(4) : 0
                color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.porcelain : LomiriColors.inkstone
                Column {
                    id: aboutColumn
                    anchors {
                        fill: parent
                        margins: units.gu(2)
                    }
                    spacing: units.gu(1)
                    width: parent.width - units.gu(4)
                    Label {
                        visible: website
                        text: i18n.tr("Website: ") + "<a href='" + website + "' style='color: DodgerBlue; text-decoration: none;'>" + website + "</a>"
                        textFormat: Label.RichText
                        width: parent.width
                        wrapMode: Label.WrapAnywhere
                        onLinkActivated: Qt.openUrlExternally(link)
                    }
                    Label {
                        text: Functions.createClickableLinks(about)
                        textFormat: Label.RichText
                        wrapMode: Label.Wrap
                        width: parent.width
                        onLinkActivated: Qt.openUrlExternally(link)
                    }
                }
            }

            Row {
                id: followRow
                spacing: units.gu(2)
                height: units.gu(4)
                anchors {
                    top: aboutRectangle.visible ? aboutRectangle.bottom : profileColumn.bottom
                    left: parent.left
                    margins: units.gu(2)
                }

                Button {
                    id: followButton
                    visible: npub != keySettings.publicKey
                    text: following ? i18n.tr("Unfollow") : i18n.tr("Follow")
                    color: following ? LomiriColors.red : LomiriColors.green

                    height: visible ? followRow.height : 0
                    width: profilePic.width

                    onClicked: {
                        if (following) {
                            let index = root.subs.indexOf(hex_pubkey);
                            root.subs.splice(index, 1);
                        }
                        else {
                            root.subs.push(hex_pubkey);
                        }
                        python.call("client.update_subs", [root.subs, keySettings.privateKey], function(){
                            if (following) {
                                following = false;
                            }
                            else {
                                following = true;
                            }
                        });
                    }
                }

                Row {
                    spacing: units.gu(1)
                    anchors.verticalCenter: parent.verticalCenter
                    Label {
                        text: followersCount > 0 ? followersCount + "+" : 0
                        font.bold: true
                    }
                    Label {
                        text: i18n.tr("Followers")
                    }
                }

                Row {
                    spacing: units.gu(1)
                    anchors.verticalCenter: parent.verticalCenter
                    Label {
                        text: followingCount
                        font.bold: true
                    }
                    Label {
                      text: i18n.tr("Following")
                    }
                }
            }

            ListView {
                id: postListView
                interactive: false

                spacing: units.gu(2)

                anchors {
                    top: followRow.bottom
                    left: parent.left
                    right: parent.right
                    topMargin: units.gu(2)
                    leftMargin: units.gu(1)
                    rightMargin: units.gu(1)
                }

                height: contentHeight + units.gu(2)

                model: postListModel

                delegate: FeedListItem {
                    profileClickable: false
                    onClicked: {
                        mainStack.push(Qt.resolvedUrl("ReplyPage.qml"), {
                            "selectedNote": postListModel.get(index),
                            "depth": 1
                        });
                    }
                }
            }

            ListModel {
                id: postListModel

                Component.onCompleted: {
                    // WORKAROUND: Remove posts that slip through to the previous page
                    profilePage.clearUnwantedPosts.connect(function(count) {
                        for (let i = 0; i < count; i++) {
                            postListModel.remove(postListModel.count - 1);
                            // console.log("removing duplicate reply");
                        }
                    });
                }
            }
        }

        flickableItem.onAtYEndChanged: {
            if (flickableItem.atYEnd && postsFetched && postListModel.count > 3 && !loadingMorePosts && fetch_time <= 1000*24*60*60) {
                // console.log("loading more posts");
                loadingMorePosts = true;
                until = since;
                since = until - fetch_time;
                oldPostCount = postListModel.count;
                python.call("client.fetch_posts", [hex_pubkey, since, until]);
            }
        }

        flickableItem.onContentYChanged: {
            // console.log(flickableItem.contentY);
            if (flickableItem.contentY > 250 && !goUp) {
                goUp = true;
            }
            else if (flickableItem.contentY <= 250 && goUp){
                goUp = false;
            }
        }
    }

    Python {
        Component.onCompleted: {
            setHandler("post_fetched", function(event_id, hex_event_id, content, date, json){
                python.call("client.fetch_reactions", [hex_event_id, hex_pubkey, "posts"]);
                python.call("client.fetch_reply_count", [hex_event_id, hex_pubkey, 1, "posts"]);

                let created_at = date;
                let combinedName = Functions.getCombinedName(name, display_name, npub);
                date = Functions.getDate(date);
                let imgURLs = Functions.getImgURLs(content);

                let imgURLsListModel = Qt.createQmlObject("import QtQuick 2.7; ListModel {}", postListModel);

                if (imgURLs.length > 0) {
                    for (let imgURL of imgURLs) {
                        imgURLsListModel.append({
                            "imgURL": imgURL
                        });
                        content = content.replace(imgURL, '');
                    }
                    if (content.length > 0 && content.replace(/\s/g, '').length == 0) {
                        content = "";
                    }
                }

                content = Functions.createClickableLinks(content);

                postListModel.append({
                    "name": name,
                    "display_name": display_name,
                    "combinedName": combinedName,
                    "event_id": event_id,
                    "npub": npub,
                    "hex_event_id": hex_event_id,
                    "hex_pubkey": hex_pubkey,
                    "content": content,
                    "created_at": created_at,
                    "date": date,
                    "json": json,
                    "picture": picture,
                    "imgURLs": imgURLsListModel,
                    "banner" : banner,
                    "website" : website,
                    "about" : about,
                    "likes": 0,
                    "liked": false,
                    "replies": 0
                });
            });

            setHandler("posts_fetched", function(){
                postsFetched = true;
                loadingMorePosts = false;
                // console.log(postListModel.count - oldPostCount);
                // console.log(fetch_time);
                if (postListModel.count - oldPostCount <= 3 && fetch_time <= 1000*24*60*60) {
                    // console.log("loading more posts");
                    loadingMorePosts = true;
                    fetch_time = fetch_time * 3;
                    until = since;
                    since = until - fetch_time;
                    python.call("client.fetch_posts", [hex_pubkey, since, until]);
                }
            });

            setHandler("got_post_reactions", function(hex_event_id, likers){
                // console.log("got ProfilePage reactions");
                for (let i = 0; i < postListModel.count; i++) {
                    if (postListModel.get(i).hex_event_id == hex_event_id) {
                        postListModel.setProperty(i, "likes", likers.length);
                        if (likers.includes(keySettings.publicKeyHex)) {
                            postListModel.setProperty(i, "liked", true);
                        }
                        break;
                    }
                }
            });

            setHandler("got_post_replies", function(hex_event_id, reply_count){
                // console.log(reply_count);
                for (let i = 0; i < postListModel.count; i++) {
                    if (postListModel.get(i).hex_event_id == hex_event_id) {
                        postListModel.setProperty(i, "replies", reply_count);
                        break;
                    }
                }
            });
        }
    }
}
