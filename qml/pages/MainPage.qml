import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import "../components"
import "../js/functions.js" as Functions

Page {
    id: mainPage

    property int fetch_time: 15*60 // Timeframe to fetch text events from as unix timestamp
    property int until
    property int since
    property bool eventsFetched: false
    property bool loadingMoreEvents: false
    property bool goUp: false
    property int likeFetchCounter: 0
    property int oldEventCount: 0

    signal updateLike(var event_id, var likes)

    Component.onCompleted: {
        if (keySettings.keySet) {
            initialLoadingRectangle.visible = true;
            until = Math.floor(Date.now() / 1000);
            since = until - fetch_time;
            python.call("client.start", [keySettings.privateKey, since, until], function(){
                initialLoadingRectangle.visible = false;
                bottomEdgeHint.status = BottomEdgeHint.Active;
                bottomEdgeHintTimer.start();
            });
        }
    }

    Component.onDestruction: {
        python.call("client.close");
        console.log("Closing connections");
    }

    header: PageHeader {
        id: header
        title: i18n.tr("NoStrut")

        trailingActionBar {
            actions: [
                Action {
                    iconName: "settings"
                    onTriggered: mainStack.push(Qt.resolvedUrl("Settings.qml"))
                },
                Action {
                    visible: eventsFetched
                    iconName: "account"
                    onTriggered: {
                        let profile_metadata = python.call_sync("client.get_metadata", [[keySettings.publicKeyHex]]);

                        let hex_pubkey = keySettings.publicKeyHex;
                        let npub = keySettings.publicKey;
                        let name = profile_metadata[0][1];
                        let display_name = profile_metadata[0][2];
                        let picture = profile_metadata[0][3];
                        let banner = profile_metadata[0][4];
                        let website = profile_metadata[0][5];
                        let about = profile_metadata[0][6];

                        mainStack.push(Qt.resolvedUrl("ProfilePage.qml"), {
                            "hex_pubkey": hex_pubkey,
                            "npub": npub,
                            "name": name,
                            "display_name": display_name,
                            "picture": picture,
                            "banner": banner,
                            "banner": banner,
                            "website": website,
                            "about": about
                        });
                    }
                },
                Action {
                    iconName: "search"
                    onTriggered: mainStack.push(Qt.resolvedUrl("SearchPage.qml"))
                }
            ]
        }
    }

    InitialLoadingRectangle {
        id: initialLoadingRectangle
        z: 1

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }

    Rectangle {
        z: 1
        id: noSubsRectangle
        visible: false
        color: "transparent"

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Label {
            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
            text: i18n.tr("Nothing to see here, follow some people by searching for pubkeys or using a different client and refresh this page. You can also find people to follow on ") + "<a href='https://www.nostr.directory/' style='color: DodgerBlue; text-decoration: none;'>https://www.nostr.directory/</a>."
            textFormat: Label.RichText
            width: units.gu(32)
            wrapMode: Label.Wrap
            horizontalAlignment: Label.AlignHCenter
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }

    BackToTopRectangle {
        MouseArea {
            anchors.fill: parent
            onClicked: feedListView.positionViewAtBeginning()
        }
    }

    ProgressBar {
        id: progressBar
        indeterminate: true
        visible: (!eventsFetched || loadingMoreEvents) && keySettings.keySet
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
        }
    }

    FeedListView {
        id: feedListView

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            topMargin: units.gu(2)
            bottomMargin: units.gu(2)
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
        }
    }

    ListModel {
        id: feedListModel

        Component.onCompleted: {
            mainPage.updateLike.connect(function(event_id, likes) {
                // console.log(event_id);
                for (let i = 0; i < feedListModel.count; i++) {
                    if (feedListModel.get(i).event_id == event_id && !feedListModel.get(i).liked) {
                        console.log("not liked, updating");
                        feedListModel.setProperty(i, "liked", true);
                        feedListModel.setProperty(i, "likes", likes);
                        break;
                    }
                }
            });
        }
    }

    PublishNoteBottomEdge {
        id: bottomEdge

        hint {
            visible: keySettings.keySet && eventsFetched
            id: bottomEdgeHint
            iconName: "note-new"
            text: i18n.tr("New Note")
        }

        Timer {
            id: bottomEdgeHintTimer
            interval: 5000
            onTriggered: bottomEdgeHint.status = BottomEdgeHint.Inactive
        }
    }

    Python {
        Component.onCompleted: {
            setHandler("event_fetched", function(event_id, npub, hex_event_id, hex_pubkey, content, date, json, name, display_name, picture, banner, website, about){

                if (likeFetchCounter < 5) {
                    python.call("client.fetch_reactions", [hex_event_id, hex_pubkey, "events"]);
                    python.call("client.fetch_reply_count", [hex_event_id, hex_pubkey, 1, "events"]);
                    likeFetchCounter += 1;
                }

                let created_at = date;
                let combinedName = Functions.getCombinedName(name, display_name, npub);
                date = Functions.getDate(date);
                let imgURLs = Functions.getImgURLs(content);

                let imgURLsListModel = Qt.createQmlObject("import QtQuick 2.7; ListModel {}", feedListModel);

                if (imgURLs.length > 0) {
                    for (let imgURL of imgURLs) {
                        imgURLsListModel.append({
                            "imgURL": imgURL
                        });
                        content = content.replace(imgURL, '');
                    }
                    if (content.length > 0 && content.replace(/\s/g, '').length == 0) {
                        content = "";
                    }
                }

                content = Functions.createClickableLinks(content);

                feedListModel.append({
                    "combinedName": combinedName,
                    "event_id": event_id,
                    "npub": npub,
                    "hex_event_id": hex_event_id,
                    "hex_pubkey": hex_pubkey,
                    "content": content,
                    "created_at": created_at,
                    "date": date,
                    "json": json,
                    "name": name,
                    "display_name": display_name,
                    "picture": picture,
                    "imgURLs": imgURLsListModel,
                    "banner" : banner,
                    "website" : website,
                    "about" : about,
                    "likes": 0,
                    "liked": false,
                    "replies": 0
                });
            });

            setHandler("events_fetched", function() {
                eventsFetched = true;
                loadingMoreEvents = false;
                if (noSubsRectangle.visible) {
                    noSubsRectangle.visible = false;
                }
                if (feedListModel.count - oldEventCount <= 5) {
                    // console.log("loading more events");
                    loadingMoreEvents = true;
                    if (fetch_time < 365*24*60*60) {
                        fetch_time = fetch_time * 3;
                    }
                    else {
                        fetch_time = 365*24*60*60;
                    }
                    until = since;
                    since = until - fetch_time;
                    python.call("client.fetch_events", [root.subs, root.subs_metadata, since, until]);
                }
            });

            setHandler("got_subs", function(subs){
                root.subs = subs;
            });

            setHandler("got_subs_metadata", function(subs_metadata){
                root.subs_metadata = subs_metadata;
            });

            setHandler("no_subs", function(){
                console.log("no subs");
                eventsFetched = true;
                noSubsRectangle.visible = true;
            });

            setHandler("got_event_reactions", function(hex_event_id, likers){
                // console.log("got MainPage reactions");
                for (let i = 0; i < feedListModel.count; i++) {
                    if (feedListModel.get(i).hex_event_id == hex_event_id) {
                        feedListModel.setProperty(i, "likes", likers.length);
                        if (likers.includes(keySettings.publicKeyHex)) {
                            feedListModel.setProperty(i, "liked", true);
                        }
                        break;
                    }
                }
            });

            setHandler("got_event_replies", function(hex_event_id, reply_count){
                // console.log(reply_count);
                for (let i = 0; i < feedListModel.count; i++) {
                    if (feedListModel.get(i).hex_event_id == hex_event_id) {
                        feedListModel.setProperty(i, "replies", reply_count);
                        break;
                    }
                }
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
