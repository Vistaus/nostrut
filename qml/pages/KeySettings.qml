import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: keySettingsPage

    property bool privateKeyVisible: false

    header: PageHeader {
        id: keySettingsHeader
        title: i18n.tr("Manage Keys")
        leadingActionBar.actions: [
            Action {
                id: backAction
                iconName: "back"
                text: "Back"
                onTriggered: mainStack.pop()
            }
        ]

        trailingActionBar {
            actions: [
                Action {
                    iconName: "reset"
                    onTriggered: {
                        PopupUtils.open(resetDialogPopup);
                    }
                }
            ]
        }
    }

    ProgressBar {
        id: progressBar
        indeterminate: true
        visible: false
        anchors {
            left: parent.left
            right: parent.right
            top: header.bottom
        }
    }

    Column {
        id: keySettingsColumn
        spacing: units.gu(4)

        anchors {
            top: keySettingsHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: units.gu(4)
        }

        PublicKeyColumn {}

        PrivateKeyColumn {}

        Label {
            id: backupKeyLabel
            visible: keySettings.keySet
            text: "Make sure to backup your private key to a secure location, as this is the only way to log into your account"
            width: keySettingsColumn.width
            wrapMode: Label.Wrap
        }
    }

    PopupRectangle {
        id: clipboardPopup
        content: ""
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
            bottomMargin: units.gu(3)
        }
    }

    Timer {
        id: hideClipboardPopupTimer
        interval: 3000
        onTriggered: clipboardPopup.visible = false;
    }

    ResetKeysDialog {
        id: resetDialogPopup
    }
}
