import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "../components"

Page {
    id: profileSettingsPage

    property string name: ""
    property string display_name: ""
    property string picture: ""
    property string banner: ""
    property string website: ""
    property string about: ""
    property bool metadataChanged: false

    header: PageHeader {
        id: profileSettingsHeader
        title: i18n.tr("Profile Settings")

        trailingActionBar {
            actions: [
                Action {
                    iconName: "ok"
                    visible: metadataChanged
                    onTriggered: {
                        python.call("client.update_metadata", [usernameTextField.text, displayNameTextField.text, pictureTextField.text, bannerTextField.text, websiteTextField.text, aboutTextArea.text], function() {
                            metadataChanged = false;
                        });
                    }
                }
            ]
        }
    }

    ScrollView {
        id: profileSettingsScrollView

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        contentItem: Rectangle {
            color: theme.palette.normal.background
            width: profileSettingsScrollView.width
            height: profileSettingsColumn.implicitHeight + units.gu(10)

            Column {
                id: profileSettingsColumn
                spacing: units.gu(3)

                anchors {
                    fill: parent
                    margins: units.gu(4)
                }

                Column {
                    spacing: units.gu(2)
                    width: parent.width

                    Label {
                        text: i18n.tr("Username:")
                    }
                    TextField {
                        id: usernameTextField
                        width: parent.width
                        height: units.gu(4)
                        text: name
                        onTextChanged: {
                            if (text != name) {
                                metadataChanged = true;
                            }
                        }
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }

                Column {
                    spacing: units.gu(2)
                    width: parent.width

                    Label {
                        text: i18n.tr("Display Name:")
                    }
                    TextField {
                        id: displayNameTextField
                        width: parent.width
                        height: units.gu(4)
                        text: display_name
                        onTextChanged: {
                            if (text != display_name) {
                                metadataChanged = true;
                            }
                        }
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }

                Column {
                    spacing: units.gu(2)
                    width: parent.width

                    Label {
                        text: i18n.tr("Profile Picture:")
                    }
                    TextField {
                        id: pictureTextField
                        width: parent.width
                        height: units.gu(4)
                        text: picture
                        onTextChanged: {
                            if (text != picture) {
                                metadataChanged = true;
                            }
                        }
                        placeholderText: i18n.tr("Enter a URL for your profile picture")
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }

                Column {
                    spacing: units.gu(2)
                    width: parent.width

                    Label {
                        text: i18n.tr("Banner:")
                    }
                    TextField {
                        id: bannerTextField
                        width: parent.width
                        height: units.gu(4)
                        text: banner
                        onTextChanged: {
                            if (text != banner) {
                                metadataChanged = true;
                            }
                        }
                        placeholderText: i18n.tr("Enter a URL for your banner image")
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }

                Column {
                    spacing: units.gu(2)
                    width: parent.width

                    Label {
                        text: i18n.tr("Website:")
                    }
                    TextField {
                        id: websiteTextField
                        width: parent.width
                        height: units.gu(4)
                        text: website
                        onTextChanged: {
                            if (text != website) {
                                metadataChanged = true;
                            }
                        }
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }

                Column {
                    spacing: units.gu(2)
                    width: parent.width

                    Label {
                        text: i18n.tr("About:")
                    }
                    TextArea {
                        id: aboutTextArea
                        width: parent.width
                        text: about
                        onTextChanged: {
                            if (text != about) {
                                metadataChanged = true;
                            }
                        }
                        anchors.horizontalCenter: parent.horizontalCenter
                        autoSize: true
                        maximumLineCount: 10
                    }
                }
            }
        }
    }


}
