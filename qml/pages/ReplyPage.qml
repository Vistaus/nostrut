import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import "../components"
import "../js/functions.js" as Functions

Page {
    id: replyPage

    property var selectedNote
    property bool repliesFetched: false
    property bool loadingMoreReplies: false
    property int depth
    property int fetch_time: 60*60 // Timeframe to fetch text events from as unix timestamp
    property int until
    property int since
    property int oldReplyCount: 1
    property bool goUp: false
    property int postCount // count of posts to be deleted
    property int replyCount: 0 // count of replies to be deleted

    signal updateLike(var event_id, var likes)
    signal clearUnwantedReplies(var count)

    Component.onCompleted: {
        replyPageListModel.append(selectedNote);
        // console.log(depth)
        if (replyPageListModel.get(0).replies > 0 && replyPageListModel.get(0).replies <= 10) {
            python.call("client.fetch_replies", [selectedNote.hex_event_id, selectedNote.hex_pubkey, 0, 0, depth]);
        }
        else if (replyPageListModel.get(0).replies > 10) {
            until = Math.floor(Date.now() / 1000);
            since = until - fetch_time;
            python.call("client.fetch_replies", [selectedNote.hex_event_id, selectedNote.hex_pubkey, since, until, depth]);
        }
        else {
            repliesFetched = true;
        }
    }

    Component.onDestruction: {
        if (String(mainStack.currentPage).startsWith("MainPage") && replyPageListModel.get(0).liked) {
            mainStack.currentPage.updateLike(replyPageListModel.get(0).event_id, replyPageListModel.get(0).likes);
        }
        else if (String(mainStack.currentPage).startsWith("ReplyPage")) {
            if (replyPageListModel.count > 1) {
                mainStack.currentPage.replyCount = replyCount + replyPageListModel.count - 1;
                mainStack.currentPage.clearUnwantedReplies(mainStack.currentPage.replyCount);
                console.log("Deleting " + mainStack.currentPage.replyCount + " unwanted Replies");
            }
            if (replyPageListModel.get(0).liked) {
                mainStack.currentPage.updateLike(replyPageListModel.get(0).event_id, replyPageListModel.get(0).likes);
            }
            mainStack.currentPage.postCount = postCount;
        }
        else if (String(mainStack.currentPage).startsWith("ProfilePage")) {
            mainStack.currentPage.postCount = postCount;
            mainStack.currentPage.clearUnwantedPosts(mainStack.currentPage.postCount);
            console.log("Deleting " + postCount + " unwanted Posts");
            mainStack.currentPage.replyCount = replyCount + replyPageListModel.count - 1;
        }
    }

    header: PageHeader {
        id: header
        title: i18n.tr("Replies")
    }

    ProgressBar {
        id: progressBar
        indeterminate: true
        visible: !repliesFetched || loadingMoreReplies
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
        }
    }

    BackToTopRectangle {
        MouseArea {
            anchors.fill: parent
            onClicked: replyPageListView.positionViewAtBeginning()
        }
    }

    ListView {
        id: replyPageListView

        spacing: units.gu(2)

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            topMargin: units.gu(2)
            bottomMargin: units.gu(2)
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
        }

        model: replyPageListModel

        onAtYEndChanged: {
            if (atYEnd && repliesFetched && !loadingMoreReplies && replyPageListModel.count - 1 < replyPageListModel.get(0).replies) {
              // console.log("loading more replies");
              loadingMoreReplies = true;
              until = since;
              since = until - fetch_time;
              oldReplyCount = replyPageListModel.count;
              python.call("client.fetch_replies", [selectedNote.hex_event_id, selectedNote.hex_pubkey, since, until, depth]);
            }
        }

        onContentYChanged: {
            let index = indexAt(0, contentY);
            if (index > 0 && !goUp) {
                goUp = true;
            }
            else if (index <= 0 && goUp && index != -1){
                goUp = false;
            }
        }

        PullToRefresh {
            id: pullToRefresh
            visible: repliesFetched
            refreshing: !repliesFetched
            onRefresh: {
                console.log("reloading replies");
                repliesFetched = false;
                replyPageListModel.clear();
                replyPageListModel.append(selectedNote);
                until = Math.floor(Date.now() / 1000);
                since = until - fetch_time;
                python.call("client.fetch_replies", [selectedNote.hex_event_id, selectedNote.hex_pubkey, since, until, depth]);
            }
        }

        delegate: FeedListItem {
            Component.onCompleted: {
                if (index == 0) {
                    isParent = true;
                }
            }
            onClicked: {
                if (!isParent) {
                    mainStack.push(Qt.resolvedUrl("ReplyPage.qml"), {
                        "selectedNote": replyPageListModel.get(index),
                        "depth": depth + 1
                    });
                }
            }
        }
    }

    ListModel {
        id: replyPageListModel

        Component.onCompleted: {
            replyPage.updateLike.connect(function(event_id, likes) {
                // console.log(event_id);
                for (let i = 0; i < replyPageListModel.count; i++) {
                    if (replyPageListModel.get(i).event_id == event_id && !replyPageListModel.get(i).liked) {
                        console.log("not liked, updating");
                        replyPageListModel.setProperty(i, "liked", true);
                        replyPageListModel.setProperty(i, "likes", likes);
                        break;
                    }
                }
            });

            // WORKAROUND: Remove replies that slip through to the previous page
            replyPage.clearUnwantedReplies.connect(function(count) {
                for (let i = 0; i < count; i++) {
                    replyPageListModel.remove(replyPageListModel.count - 1);
                    // console.log("removing duplicate reply");
                }
            });
        }
    }

    Python {
        Component.onCompleted: {
            setHandler("reply_fetched", function(event_id, npub, hex_event_id, hex_pubkey, content, date, json, name, display_name, picture, banner, website, about){
                python.call("client.fetch_reactions", [hex_event_id, hex_pubkey, "replies"]);
                python.call("client.fetch_reply_count", [hex_event_id, hex_pubkey, depth + 1, "replies"]);

                let created_at = date;
                let combinedName = Functions.getCombinedName(name, display_name, npub);
                date = Functions.getDate(date);
                let imgURLs = Functions.getImgURLs(content);

                let imgURLsListModel = Qt.createQmlObject("import QtQuick 2.7; ListModel {}", replyPageListModel);

                if (imgURLs.length > 0) {
                    for (let imgURL of imgURLs) {
                        imgURLsListModel.append({
                            "imgURL": imgURL
                        });
                        content = content.replace(imgURL, '');
                    }
                    if (content.length > 0 && content.replace(/\s/g, '').length == 0) {
                        content = "";
                    }
                }

                content = Functions.createClickableLinks(content);

                replyPageListModel.append({
                    "combinedName": combinedName,
                    "event_id": event_id,
                    "npub": npub,
                    "hex_event_id": hex_event_id,
                    "hex_pubkey": hex_pubkey,
                    "content": content,
                    "created_at": created_at,
                    "date": date,
                    "json": json,
                    "name": name,
                    "display_name": display_name,
                    "picture": picture,
                    "imgURLs": imgURLsListModel,
                    "banner" : banner,
                    "website" : website,
                    "about" : about,
                    "likes": 0,
                    "liked": false,
                    "replies": 0
                });
            });

            setHandler("replies_fetched", function() {
                repliesFetched = true;
                loadingMoreReplies = false;
                if (replyPageListModel.count - 1 < replyPageListModel.get(0).replies && until <= Math.floor(Date.now() / 1000) && replyPageListModel.count - oldReplyCount <= 3) {
                    // console.log("loading more replies");
                    loadingMoreReplies = true;
                    if (fetch_time < 365*24*60*60) {
                        fetch_time = fetch_time * 10;
                    }
                    else {
                        fetch_time = 365*24*60*60;
                    }
                    until = since;
                    since = until - fetch_time;
                    python.call("client.fetch_replies", [selectedNote.hex_event_id, selectedNote.hex_pubkey, since, until, depth]);
                }
            });

            setHandler("got_reply_reactions", function(hex_event_id, likers){
                // console.log("got ReplyPage reactions");
                for (let i = 0; i < replyPageListModel.count; i++) {
                    if (replyPageListModel.get(i).hex_event_id == hex_event_id) {
                        replyPageListModel.setProperty(i, "likes", likers.length);
                        if (likers.includes(keySettings.publicKeyHex)) {
                            replyPageListModel.setProperty(i, "liked", true);
                        }
                        break;
                    }
                }
            });

            setHandler("got_reply_replies", function(hex_event_id, reply_count){
                // console.log(reply_count);
                for (let i = 0; i < replyPageListModel.count; i++) {
                    if (replyPageListModel.get(i).hex_event_id == hex_event_id) {
                        replyPageListModel.setProperty(i, "replies", reply_count);
                        break;
                    }
                }
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
