import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Page {
    id: setupPage

    property bool newUser: true
    property bool loading: false

    Component.onCompleted: root.anchorToKeyboard = false
    Component.onDestruction: root.anchorToKeyboard = true

    header: PageHeader {
        id: header
        title: i18n.tr("NoStrut")
    }

    Column {
        spacing: units.gu(3)
        anchors {
            centerIn: parent
        }
        Row {
            spacing: units.gu(1)
            anchors.horizontalCenter: parent.horizontalCenter
            Icon {
                visible: !newUser
                name: newUser ? "" : "stock_key"
                height: units.gu(2)
                width: height
            }
            Label {
                text: newUser ? i18n.tr("Enter a username:") : i18n.tr("Enter your private key:")
            }
        }
        TextField {
            id: textField
            width: units.gu(32)
            height: units.gu(4)
            placeholderText: newUser ? "" : i18n.tr("nsec...")
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Button {
            visible: !loading
            text: i18n.tr("Get Started")
            width: units.gu(16)
            height: units.gu(4)
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: {
                if (newUser && textField.text) {
                    if (textField.text.startsWith("nsec1")) {
                        PopupUtils.open(invalidUserNameDialogPopup);
                    }
                    else {
                        loading = true;
                        python.call("keys.generate_keys", [], function(keys) {
                            keySettings.privateKey = keys[0];
                            keySettings.publicKey = keys[1];
                            keySettings.publicKeyHex = keys[2];
                            keySettings.keySet = true;
                            python.call("client.update_username", [keySettings.privateKey, textField.text]);
                        });
                    }
                }
                if (!newUser) {
                    python.call("keys.generate_publickey", [textField.text]);
                }
            }
        }
        ActivityIndicator {
            visible: loading
            height: units.gu(3)
            width: units.gu(3)
            id: loadingIndicator
            running: true
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Label {
            visible: newUser
            text: i18n.tr("You can always change it later")
            textSize: Label.Small
            width: parent.width
            horizontalAlignment: Label.AlignHCenter
        }
    }

    Label {
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            bottomMargin: units.gu(3)
        }
        text: newUser ? i18n.tr("Already using Nostr?") : i18n.tr("Create new user?")
        horizontalAlignment: Label.AlignHCenter

        MouseArea {
            enabled: !loading
            anchors.fill: parent
            onClicked: {
                if (newUser) {
                    newUser = false;
                }
                else {
                    newUser = true;
                }
                textField.focus = false;
                textField.text = "";
            }
        }
    }

    Component {
        id: invalidPrivateKeyDialogPopup
        Dialog {
            id: invalidPrivateKeyDialog
            title: i18n.tr("Invalid private key")
            text: i18n.tr("The private key appears to be invalid, please enter a correct private key prefixed with nsec")
            Button {
                text: i18n.tr("Close")
                color: LomiriColors.red
                onClicked: {
                    textField.text = "";
                    PopupUtils.close(invalidPrivateKeyDialog)
                }
            }
        }
    }

    Component {
        id: invalidUserNameDialogPopup
        Dialog {
            id: invalidUserNameDialog
            title: i18n.tr("Invalid username")
            text: i18n.tr("The username you entered is not valid, it may contain a private key or is otherwise not suitable. Please try a different one")
            Button {
                text: i18n.tr("Close")
                color: LomiriColors.red
                onClicked: {
                    textField.text = "";
                    PopupUtils.close(invalidUserNameDialog)
                }
            }
        }
    }

    Python {
        Component.onCompleted: {
            setHandler("invalid_privatekey", function(){
                PopupUtils.open(invalidPrivateKeyDialogPopup);
            });

            setHandler("publickey_generated", function(privatekey, pubkey, hex_pubkey){
                keySettings.privateKey = privatekey;
                keySettings.publicKey = pubkey;
                keySettings.publicKeyHex = hex_pubkey;
                keySettings.keySet = true;
                general.setupCompleted = true;
                mainStack.pop();
                mainStack.push(Qt.resolvedUrl("MainPage.qml"));
            });

            setHandler("username_updated", function(){
                general.setupCompleted = true;
                loading = false;
                mainStack.pop();
                mainStack.push(Qt.resolvedUrl("MainPage.qml"));
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
